import React, {Component} from 'react';
import "./ProductCreate.less"

class ProductCreate extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      name: "",
      price: 0,
      unit: "",
      image: "",
      addSuccess: false
    };

    this.getImageUrl = this.getImageUrl.bind(this);
    this.getNameValue = this.getNameValue.bind(this);
    this.getPriceValue = this.getPriceValue.bind(this);
    this.getUnitValue = this.getUnitValue.bind(this);
    this.addProduct = this.addProduct.bind(this);
  }

  render() {
    return (
      <div>
        <form className="product-add">
          <h2>添加商品</h2>
          <label htmlFor="">名称<input className="input" type="text" onChange={this.getNameValue} placeholder="名称"
                                     required/></label>
          <label htmlFor="">价格<input className="input" type="text" onChange={this.getPriceValue} placeholder="价格"
                                     required/></label>
          <label htmlFor="">单位<input className="input" type="text" onChange={this.getUnitValue} placeholder="单位"
                                     required/></label>
          <label htmlFor="">图片<input className="input" type="text" onChange={this.getImageUrl} placeholder="URL"
                                     required/></label>
          <button type="submit" disabled={this.state.addSuccess} onClick={this.addProduct}
                  className="product-add-button">提交
          </button>
        </form>
      </div>
    );
  }


  addProduct(event) {
    event.preventDefault();
    let product = {
      name: this.state.name,
      price: this.state.price,
      unit: this.state.unit,
      image: this.state.image
    };
    fetch("http://localhost:8080/api/products", {
      method: "PUT",
      headers: {'content-type': 'application/json'},
      body: JSON.stringify(product)
    }).then(res => {
      res.json().then(() => {
        this.setState({addSuccess: false});
      })
    });
  }

  getNameValue(event) {
    this.setState({name: event.target.value});
  }

  getPriceValue(event) {
    this.setState({price: +event.target.value});
  }

  getUnitValue(event) {
    this.setState({unit: event.target.value});
  }

  getImageUrl(event) {
    this.setState({image: event.target.value});
  }
}

export default ProductCreate;
