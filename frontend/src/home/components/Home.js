import React, {Component} from 'react';
import {connect} from 'react-redux';
import ProductList from "./ProductList";
import {Link} from "react-router-dom"

function mapStateToProps(state) {
  return {};
}

class Home extends Component {


  componentDidMount() {

  }

  render() {
    return (
      <div>
        <nav>
          <ul>
            <Link to="/">商城</Link>
            <li>订单</li>
            <Link to="/products/add">添加商品</Link>
          </ul>
        </nav>
        <ProductList/>
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
)(Home);
