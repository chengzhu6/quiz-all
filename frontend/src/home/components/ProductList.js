import React, {Component} from 'react';
import {connect} from 'react-redux';
import Item from "./Item";
import "./ProductList.less"

function mapStateToProps(state) {
  return {};
}


class ProductList extends Component {


  constructor(props, context) {
    super(props, context);
    this.state = {
      products: []
    }
  }


  componentDidMount() {
    fetch("http://localhost:8080/api/products").then(response => {
      response.json().then(r => {
        this.setState({products: r})
        }
      );
    });
  }

  render() {
    return (
      <div>
        <ul className="product-list">
          {this.state.products.map(item => {
            return (<Item product={item}/>)
          })}
        </ul>


      </div>
    );
  }
}

export default connect(
  mapStateToProps,
)(ProductList);
