import React, {Component} from 'react';
import "./Item.less"

class Item extends Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <li className="product-item">
        <img src={this.props.product.image} alt=""/>
        <p>{this.props.product.name}</p>
        <p>单价¥<span>{this.props.product.price}</span>/<span>{this.props.product.unit}</span></p>
      </li>
    );
  }
}

export default Item;
