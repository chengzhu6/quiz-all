import React, {Component} from 'react';
import './App.less';
import Home from "./home/components/Home";
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import ProductCreate from "./home/components/ProductCreate";


class App extends Component{
  render() {
    return (
      <div className='App'>
        <Router>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/products/add" component={ProductCreate} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
