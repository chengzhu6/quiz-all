package com.twuc.webApp.service;

import com.twuc.webApp.entity.Order;
import com.twuc.webApp.entity.Product;
import com.twuc.webApp.repository.OrderRepo;
import com.twuc.webApp.repository.ProductRepo;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class OrderServiceTest {


    @Test
    void should_save_order() {
        OrderRepo orderStub = mock(OrderRepo.class);
        ProductRepo productRepoStub = mock(ProductRepo.class);

        OrderService orderService = new OrderService(orderStub, productRepoStub);
        Long productId = 1L;
        Product product = new Product();
        when(productRepoStub.findById(productId)).thenReturn(java.util.Optional.of(product));
        Order order = new Order(product, 1);
        when(orderStub.save(order)).thenReturn(order);

        orderService.save(productId, 1);

        verify(orderStub).save(order);



    }
}
