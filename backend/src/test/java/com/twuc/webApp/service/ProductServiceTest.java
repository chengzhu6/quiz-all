package com.twuc.webApp.service;

import com.twuc.webApp.contract.ProductRequest;
import com.twuc.webApp.entity.Product;
import com.twuc.webApp.repository.ProductRepo;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ProductServiceTest {


    @Test
    void should_get_all_products() {
        ProductRepo productRepoStub = mock(ProductRepo.class);
        ProductService productService = new ProductService(productRepoStub);

        List<Product> products = Collections.singletonList(new Product());
        when(productRepoStub.findAll()).thenReturn(products);
        List<Product> productList = productService.getProductList();
        assertEquals(products, productList);
    }


    @Test
    void should_save_product() {
        ProductRepo productRepoMock = mock(ProductRepo.class);
        ProductRequest productRequest = new ProductRequest("cola", "瓶" , "url", BigDecimal.valueOf(10));
        Product product = ProductRequest.createProduct(productRequest);
        ProductService productService = new ProductService(productRepoMock);
        when(productRepoMock.save(product)).thenReturn(product);

        productService.saveProduct(productRequest);


        verify(productRepoMock).save(product);
    }
}
