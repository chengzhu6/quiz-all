package com.twuc.webApp.web;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.contract.ProductRequest;
import com.twuc.webApp.entity.Product;
import com.twuc.webApp.repository.ProductRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;


class ProductControllerTest extends ApiTestBase {



    @Autowired
    ProductRepo productRepo;

    @Test
    void should_get_products() throws Exception {
        Product savedProduct = productRepo.save(new Product("product", BigDecimal.valueOf(10), "瓶", "imageUrl"));
        List<Product> products = Arrays.asList(savedProduct);

        String productsJson = serialize(products);
        mockMvc.perform(get("/api/products"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(productsJson));
    }


    @Test
    void should_save_product() throws Exception {
        ProductRequest productRequest = new ProductRequest("cola", "瓶" , "url", BigDecimal.valueOf(10));
        String requestJson = serialize(productRequest);

        mockMvc.perform(put("/api/products").contentType(MediaType.APPLICATION_JSON_UTF8).content(requestJson))
                .andExpect(MockMvcResultMatchers.status().is(201));
    }

    @Test
    void should_given_400_when_product_info_contains_null() throws Exception {
        ProductRequest productRequest = new ProductRequest();
        String requestJson = serialize(productRequest);

        mockMvc.perform(put("/api/products").contentType(MediaType.APPLICATION_JSON_UTF8).content(requestJson))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }
}
