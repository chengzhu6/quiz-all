create table product (
       id bigint NOT NULL AUTO_INCREMENT,
        image varchar(255) not null,
        name varchar(64) not null,
        price decimal(19,2) not null,
        unit varchar(10) not null,
        primary key (id)
    ) CHARSET=utf8;
