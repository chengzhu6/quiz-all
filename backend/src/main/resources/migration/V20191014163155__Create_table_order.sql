create table `order` (
       id bigint NOT NULL AUTO_INCREMENT,
        `quantity` INTEGER ,
        `product_id` bigint,
        primary key (id)
    );

alter table `order`
       add constraint FK3b51sphq5i3qy7iai0o5lmu7l
       foreign key (product_id)
       references product (id);
