package com.twuc.webApp.service;


import com.twuc.webApp.entity.Order;
import com.twuc.webApp.entity.Product;
import com.twuc.webApp.repository.OrderRepo;
import com.twuc.webApp.repository.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OrderService {

    private final OrderRepo orderRepo;

    private final ProductRepo productRepo;


    public OrderService(OrderRepo orderRepo, ProductRepo productRepo) {
        this.orderRepo = orderRepo;
        this.productRepo = productRepo;
    }

    public void save(Long productId, int quantity) {

        Product product = productRepo.findById(productId).orElse(null);
        if (product == null) {
            throw new IllegalArgumentException();
        }
        Order order = new Order(product, quantity);
        orderRepo.save(order);
    }
}
