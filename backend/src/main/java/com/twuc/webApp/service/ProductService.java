package com.twuc.webApp.service;


import com.twuc.webApp.contract.ProductRequest;
import com.twuc.webApp.entity.Product;
import com.twuc.webApp.repository.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    private final ProductRepo productRepo;

    public ProductService(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }


    public List<Product> getProductList() {
        return productRepo.findAll();
    }


    public void saveProduct(ProductRequest productRequest) {
        Product product = ProductRequest.createProduct(productRequest);
        productRepo.save(product);

    }
}
