package com.twuc.webApp.contract;

import com.twuc.webApp.entity.Product;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class ProductRequest {


    @NotNull
    private String name;

    @NotNull
    private String unit;

    @NotNull
    private String image;

    @NotNull
    private BigDecimal price;


    public ProductRequest() {
    }

    public static Product createProduct(ProductRequest productRequest) {
        return new Product(productRequest.getName(), productRequest.getPrice(), productRequest.getUnit(), productRequest.getImage());
    }

    public ProductRequest(String name, String unit, String image, BigDecimal price) {
        this.name = name;
        this.unit = unit;
        this.image = image;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public String getImage() {
        return image;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
