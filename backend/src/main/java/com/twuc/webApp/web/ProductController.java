package com.twuc.webApp.web;


import com.twuc.webApp.contract.ProductRequest;
import com.twuc.webApp.entity.Product;
import com.twuc.webApp.service.ProductService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/products")
@CrossOrigin(origins = "*", exposedHeaders = HttpHeaders.LOCATION)
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    ResponseEntity getProducts(){
        List<Product> productList = productService.getProductList();
        return ResponseEntity.ok().body(productList);
    }


    @PutMapping
    ResponseEntity saveProduct(@RequestBody @Valid ProductRequest productRequest) {
        productService.saveProduct(productRequest);
        return ResponseEntity.status(201).build();
    }


}
