Story 1
AC 1
1. 用户访问商品列表，应该返回商品list信息。
    1. create product entity.
    2. create product table.
    3. create product repository, can find all products.
    4. create product service, call product repository.
    5. create product controller, impl method call product service return product list.
Story 2
Ac 1 
1. 给一个完整的商品信息，应该能都添加商品信息到数据库中。
    1. create product contract.
    2. product service add method to save product info.
    3. product controller add method to call product service add product.
2. 给定的商品信息各个field不能为空。

Story 3
Ac 1 
1 添加一个商品到订单中
    1. create order entity.
    2. create order table.
    3. create order repository, can save a order.
    4. create order service, create method save a order.
    5. create order controller, create method call service method save order.
    
